# Case__fly

> It's a front-end template using pug/scss/gulp/bower/svg-sprites

## How to install

Prerequisites:
* [Node.js](http://nodejs.org/) (ver. 5.12.0)
* [Bower](http://bower.io/) 

Installation process:

1. Clone this repository
2. Run ```npm i``` to install dependencies
3. Run ```bower i``` to install front-end dependencies

## Usage

```
Use folder app/sprites/ to build svg-sprites
```

For project development with livereload run:
```
gulp serve
```

To build project run: (Result will be in ```dist/``` folder.)
```
gulp build [--force] 
```
