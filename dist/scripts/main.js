'use strict';

// ready
$(document).ready(function () {

    // adaptive
    $('.main-nav__toggle--js').click(function () {
        $(this).closest('.header-top').toggleClass('active');
        $('body').toggleClass('fix');
    });
    $('.aside-mobile--js').click(function () {
        $(this).parent().toggleClass('active');
        $('body').toggleClass('fix');
    });
    $('.footer-nav__toggle--js').click(function () {
        $(this).parent().toggleClass('active');
    });
    $('.header-search--js').click(function () {
        $(this).next().slideToggle();
        return false;
    });
    $('.header-search__close--js').click(function () {
        $(this).parent().slideToggle();
    });
    // adaptive

    // @require FlipClock
    $('.b-countdown').each(function () {
        var dateFlip = $(this).find('div').attr('data-time');
        var clock = $(this).FlipClock({
            clockFace: 'DailyCounter',
            autoStart: false,
            callbacks: {
                stop: function stop() {
                    $('.cclock').html('Акция завершена!');
                }
            }
        });
        clock.setTime(dateFlip);
        clock.setCountdown(true);
        clock.start();
    });
    /* End */

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    $('.main-slider').slick({
        speed: 500,
        cssEase: 'linear'
    });
    $('.blog-slider').slick({
        speed: 500,
        dots: true,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 480,
            settings: {
                arrows: false
            }
        }]
    });
    $('.sidebar-slider').slick({
        speed: 500,
        vertical: true,
        prevArrow: $('.slider-arrow-left'),
        nextArrow: $('.slider-arrow-right'),
        cssEase: 'linear'
    });
    $('.sidebar-slider-mult').slick({
        speed: 500,
        vertical: true,
        slidesToShow: 3,
        // center: true,
        prevArrow: $('.slider-arrow-left'),
        nextArrow: $('.slider-arrow-right'),
        cssEase: 'linear'
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        focusOnSelect: true
    });
    // slider

    // popup {magnific-popup}
    $('.popup').magnificPopup({
        showCloseBtn: false,
        callbacks: {
            beforeOpen: function beforeOpen() {
                this.wrap.removeAttr('tabindex');
            }
        }
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function opener(element) {
                return element.find('img');
            }
        }
    });
    // popup

    //tags
    $('.close-tag--js').click(function () {
        $(this).parent().hide('slow').remove();
    });
    //tags

    //tabs
    $('.tabs__caption--js').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active').closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('.radio--js').on('click', 'input', function () {
        var radioValue = $(this).val();
        var radioContent = $('.is-card');
        if (radioValue == 'card') {
            radioContent.show();
        } else {
            radioContent.hide();
        }
    });
    //tabs

    //accordion filter
    $('.accordion-item-js').filter(':not(.active)').hide();
    $('.accordion-title-js .accordion-title').click(function () {
        $(this).parent().find('.accordion-item-js:first').stop(true, true).slideToggle().parent().toggleClass('active');
        if ($('.accordion-title-js').hasClass('active')) {
            setTimeout(function () {}, 1);
        }
        return false;
    });
    $('.show-title-js').click(function () {
        $(this).next().slideToggle('');
    });
    $('.search_btn-js').click(function () {
        $(this).parent().prev().addClass('active');
        return false;
    });
    //accordion filter

    $(function () {
        $('.check_all').change(function () {
            $(this).closest('.accordion-inner').find('.chk--js').prop('checked', this.checked);
        });
    });
    $(function () {
        $('.check-tag--js').change(function () {
            var tagName = $(this).attr('data-target');
            var tagClass = $(this).attr('data-class');
            if ($(this).prop('checked')) {
                $('.tags').append('<div class="tags-item" data-id="' + tagName + '"><button class="btn ' + tagClass + ' close-tag--js">' + tagName + '</button></div>');
            } else {
                $('.tags').find('.tags-item[data-id="' + tagName + '"]').remove();
            }
        });
    });
    //tags

    //popover
    $('.target--js:not(.disabled)').mouseover(function () {
        var attr = $(this).attr('data-popover');
        var position = $(this).position();
        if (typeof attr !== typeof undefined && attr !== false) {
            $('.popover').addClass('active').css({ 'left': position.left - 20, 'top': position.top + 50 }).text(attr);
        }
    }).mouseout(function () {
        var attr = $(this).attr('data-popover');
        if (typeof attr !== typeof undefined && attr !== false) {
            $('.popover').removeClass('active');
        }
    });
    //popover

    // select {select2}
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    $('#search-map').select2({});
    // select
});
// ready

// load
$(document).load(function () {});
// load

// scroll
$(window).scroll(function () {
    var sticky = $('.header-top'),
        scroll = $(window).scrollTop();

    if (scroll >= 65) sticky.addClass('fixed');else sticky.removeClass('fixed');
});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts
//# sourceMappingURL=main.js.map
